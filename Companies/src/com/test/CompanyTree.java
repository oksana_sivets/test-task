package com.test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

public class CompanyTree {
	
	private DbCompany dbComnpany;  
	List<Company> companies = new ArrayList<Company>();
	
	public CompanyTree() throws SQLException, ServletException, ClassNotFoundException{
		dbComnpany = new DbCompany();
	}
	 
	public List<Company> getCompaniesList() throws SQLException, ServletException{
		companies = dbComnpany.loadCompanies();
    	calculateTotalEE();
    	return companies;
    }
	
	public String generateCompRow(Company company){
		String child_node = "";
		if(hasChild(company.getId())){
			child_node += "<ul>";
			for(Company child : getChilds(company.getId())){
				child_node += generateCompRow(child);
			}
			child_node += "</ul>";
		}
		return "<li><span><a>" + company.getName() + " : EE = " + company.getEstimatedEarnings() +  "; Total EE =" + company.getTotalEE() + "</a></span>" + child_node + "</li>";
	}
    
    private void calculateTotalEE(){
    	for (Company company : companies) {
    		if(company.getParent_id() == 0){
    			calculeteEETree(company);
    		}
    	}
    }
    
    private int calculeteEETree(Company company){
    	int ee = company.getEstimatedEarnings();
    	if(hasChild(company.getId())){
    		for(Company child : getChilds(company.getId())){
				ee += calculeteEETree(child);
			}
    	}
    	company.setTotalEE(ee);
    	return ee;
    }    
    
    
    public String getCompNameById(int id){
    	for(Company comp : companies){
    		if( comp.getId() == id){
    			return comp.getName();
    		}
    	}
    	return "";
    }
    
    public Boolean hasChild(int id){
    	for(Company comp : companies){
    		if( comp.getParent_id() == id){
    			return true;
    		}
    	}
    	return false;
    }
    
    public List<Company> getChilds(int id){
    	List<Company> comps_list = new ArrayList<Company>();
    	for(Company comp : companies){
    		if( comp.getParent_id() == id){
    			comps_list.add(comp);
    		}
    	}    	
    	return comps_list;
    }

    public Company getCompanyById(int id){
    	for(Company company : companies){
    		if( company.getId() == id){
    			return company;
    		}
    	}
    	return null;
    }
    
    public int getIdByName(String name){
    	for(Company company : companies){
    		if( company.getName() == name){
    			return company.getId();
    		}
    	}
    	return 0;
    }
}
