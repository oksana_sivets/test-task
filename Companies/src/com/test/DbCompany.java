package com.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
//import java.sql.*;  
//import com.microsoft.sqlserver.jdbc.*;

import javax.servlet.ServletException;

public class DbCompany {

    private Connection dbconnection = null;
    private ResultSet resultSet = null;
    private PreparedStatement pstatement = null;
    
    List<Company> companies = new ArrayList<Company>();
    
    private void getConnection() throws ClassNotFoundException, SQLException{
    	Class.forName("org.postgresql.Driver");
    	dbconnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/Companies", "postgres", "postgres");
		
    }   
    public DbCompany() throws ClassNotFoundException, SQLException {
    	getConnection();
    
    	pstatement = dbconnection.prepareStatement("CREATE TABLE IF NOT EXISTS companies " +
    			"(id INTEGER not NULL, " +
    			" name VARCHAR(64), " + 
    			" estimated_earnings INTEGER, " + 
    			" parent_id INTEGER, " + 
    			" PRIMARY KEY ( id ))");
    			pstatement.executeUpdate();
    	
    }

    public List<Company> loadCompanies() throws SQLException, ServletException {
        Company company = null;
        companies.clear();
        try {
        	getConnection();
            pstatement = dbconnection.prepareStatement("SELECT * FROM companies");
            
            resultSet = pstatement.executeQuery();

            while (resultSet.next()) {
                company = new Company();
                company.setId(resultSet.getInt("id"));
                company.setName(resultSet.getString("name"));
                company.setEstimatedEarnings(resultSet.getInt("estimated_earnings"));
                company.setParent_id(resultSet.getInt("parent_id"));
                companies.add(company);                
            }
        }

        catch (ClassNotFoundException ex)
        {
               System.err.println("ClassNotFoundException: " + ex.getMessage());
               throw new ServletException("Class not found Error error"  + ex.getMessage());
        }
        catch (SQLException ex)
        {
               System.err.println("SQLException: " + ex.getMessage());
        }
    
        finally {
            if (resultSet != null) {
                resultSet.close();
            }
            if (pstatement != null) {
                pstatement.close();
            }
            if (dbconnection != null && !dbconnection.isClosed()) {
            	dbconnection.close();
            }

        }
        return companies;
    }
    
    public void addCompany(Company company) throws SQLException, ClassNotFoundException{
    	getConnection();
    	int parent_id = company.getParent_id();
    	if(parent_id != 0){
    		pstatement = dbconnection.prepareStatement("INSERT INTO companies(name, estimated_earnings, parent_id) values(?,?,?)");
    		pstatement.setInt(3, parent_id);
    	}
    	else {
    		pstatement = dbconnection.prepareStatement("INSERT INTO companies(name, estimated_earnings) values(?,?)");
    	}
    	pstatement.setString(1, company.getName());
    	pstatement.setInt(2, company.getEstimatedEarnings());
    	

    	pstatement.executeUpdate();
    	dbconnection.close();
    }
    
    public void deleteCompany(int id) throws SQLException, ClassNotFoundException{
    	getConnection();

    	pstatement = dbconnection.prepareStatement("UPDATE companies SET parent_id=null WHERE parent_id=?");
    	pstatement.setInt(1, id);
    	pstatement.executeUpdate();
    	
    	pstatement = dbconnection.prepareStatement("DELETE FROM companies WHERE id=?");
    	pstatement.setInt(1, id);
    	pstatement.executeUpdate();
    	
    	dbconnection.close();
    }
    
    public void editCompany(int id, String name, int EE, int parent_id) throws SQLException, ClassNotFoundException{
    	getConnection();
    	
    	pstatement = dbconnection.prepareStatement("UPDATE companies SET name=?, estimated_earnings=?, parent_id=? WHERE id=?");
    	
    	if(parent_id == 0){
    		pstatement.setNull(3, Types.INTEGER);
    	}
    	else {
    		pstatement.setInt(3, parent_id);
    	}
    	pstatement.setString(1, name);
    	pstatement.setInt(2, EE);
    	pstatement.setInt(4, id);
    	
    	
    	pstatement.executeUpdate();
    	
    	dbconnection.close();
    }
}
