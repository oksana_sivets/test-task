package com.test;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



@WebServlet("/CompanyServlet")
public class CompanyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    private CompanyTree companyTree;
    private DbCompany dbCompany;
    List<Company> companies = new ArrayList<Company>();
    
    public CompanyServlet() throws SQLException, ServletException, ClassNotFoundException {
        super();
        companyTree = new CompanyTree();
        dbCompany = new DbCompany();
    }

	
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
    throws ServletException, IOException { 
       
           
      HttpSession session = request.getSession();
      
      try {
			companies = companyTree.getCompaniesList();
		}
      catch (SQLException e) { 
		// TODO Auto-generated catch block
		e.printStackTrace();
      }
      String target = "";

      if (request.getParameter("target") != null) {
    	  target = request.getParameter("target");
      }
      
      if(target.equals("getTable")) {
                session.setAttribute("companies", companies);
                session.setAttribute("companyTree", companyTree);
    		
    	  RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
    	            "/companies_table.jsp"); 
    	      rDispatch.forward(request, response);  
      }
      else if(target.equals("getTree")) {
    	  PrintWriter pw = response.getWriter();
  		
  		pw.println("<ul>");
  		for (Company company : companies) {
  			if(company.getParent_id() == 0){
  				pw.println(companyTree.generateCompRow(company));
  			}
  		}
  		pw.println("</ul>");
      }
      else if(target.equals("getForm"))
      {
          session.setAttribute("companies", companies);
          RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
                "/form.jsp"); 
          rDispatch.forward(request, response);
      }
      else if(target.equals("editCompany"))
      {
    	  int comp_id = Integer.parseInt(request.getParameter("compId"));
    	  Company comp = companyTree.getCompanyById(comp_id);
    	  
          session.setAttribute("companies", companies);
          
          request.setAttribute("action", "editCompany");
          request.setAttribute("comp_id", comp_id);
          request.setAttribute("companyName", comp.getName());
          request.setAttribute("estimatedEarnings", comp.getEstimatedEarnings());
          request.setAttribute("parentCompanyId", comp.getParent_id());
          RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
                "/form.jsp"); 
          rDispatch.forward(request, response);
      }
      else {
      
      RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
            "/companies.jsp"); 
      rDispatch.forward(request, response);
      }
   } 
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    	    throws ServletException, IOException {
    	
    	String action = request.getParameter("action");

    	if(action.equals("addCompany")){
    		String name = request.getParameter("companyName");
    		int estimatedEarnings = Integer.parseInt(request.getParameter("estimatedEarnings"));
    		int parent_id = Integer.parseInt(request.getParameter("parentCompanyId"));
		
    			Company newCompany = new Company(name, estimatedEarnings, parent_id);
    			try {			
    				dbCompany.addCompany(newCompany);
    			
                		    		
    				RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
		    	            "/"); 
    				rDispatch.forward(request, response);
    			} catch (SQLException | ClassNotFoundException e) {
    				e.printStackTrace();
    			}
    	}
    	else if(action.equals("delCompany")){ 
    		int comp_id = Integer.parseInt(request.getParameter("companyId"));
    		
    		try {
				dbCompany.deleteCompany(comp_id);
			} catch (ClassNotFoundException | SQLException e) {
			
				e.printStackTrace();
			}
    	}
    	else if(action.equals("editCompany")){
    		
    		int comp_id = Integer.parseInt(request.getParameter("comp_id"));
    		String name = request.getParameter("companyName");
    		int estimatedEarnings = Integer.parseInt(request.getParameter("estimatedEarnings"));
    		int parent_id = Integer.parseInt(request.getParameter("parentCompanyId"));
		
    		try {
				dbCompany.editCompany(comp_id, name, estimatedEarnings, parent_id);
			} catch (ClassNotFoundException | SQLException e) {
				
				e.printStackTrace();
			}
    		
    		
			RequestDispatcher rDispatch = getServletContext().getRequestDispatcher( 
    	            "/"); 
			rDispatch.forward(request, response);
	
    	}
    }

}
