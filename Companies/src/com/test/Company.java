package com.test;

public class Company {

	private int id;
	private String name;
	private int estimatedEarnings;
	private int parent_id;
	private int totalEE;
	
	public Company(int id, String name, int estimatedEarnings, int parent_id){
		this.id = id;
		this.name = name;
		this.estimatedEarnings = estimatedEarnings;
		this.parent_id = parent_id;
	}
	
	public Company(){
		
	}
	
	public Company(String name, int estimatedEarnings, int parent_id){
		
		this.name = name;
		this.estimatedEarnings = estimatedEarnings;
		this.parent_id = parent_id;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEstimatedEarnings() {
		return estimatedEarnings;
	}
	public void setEstimatedEarnings(int estimatedEarnings) {
		this.estimatedEarnings = estimatedEarnings;
	}
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}

	public int getTotalEE() {
		return totalEE;
	}

	public void setTotalEE(int totalEE) {
		this.totalEE = totalEE;
	}
}
