<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="companiesApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="shortcut icon" href="">
<title>Companies estimated earnings</title>
<script src="js/angular.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.28//angular-route.min.js"></script>
<script src="js/CompaniesController.js"></script>

<style type="text/css">
TABLE {
    border-collapse: collapse; 
   }
   TD, TH {
    padding: 3px; 
    border: 1px solid black;
   }
   #tree-container{
   	display: inline-block;
   	margin: 0 auto;
   }
   
   ul {
    text-align: left;
    padding: 0;
    margin-left: 20px;
   }
   
</style>
</head>
<body>

<center>
	<div  ng-controller="CompaniesController">
	
	<div style="padding-bottom: 10px;">
		<button id="show-button" ng-click="showBtn()">{{btnText}}</button>
		<button id="show-form" ng-click="showForm()">Add Company</button>
	</div>	
  <table ng-show ="companiesTable">
	<tr> 
      <th>Company Name</th> 
      <th>Estimated Earnings</th> 
      <th>Parent Company</th>
      <th>Total Estimated Earnings</th> 
      <th>Actions</th> 
   </tr>
  
   <tbody table-directive id="companies-table-body">
   
   </tbody>
   </table>
    	
	<div id="tree-container"  ng-show="companiesTree"></div>
	<div id="add-company-form" ng-show="companyForm"></div>
	</div>
	
</center>


</body>
</html>