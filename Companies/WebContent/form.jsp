<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>

	<form method="post" action="CompanyServlet" name="CompanyForm">
	<input type="hidden" name="action"
	value="${not empty action ? action : "addCompany"}" 
		 />
		 <c:if test="${not empty comp_id}">
			<input type="hidden" name="comp_id" value="${comp_id }"/>	 
		 </c:if>
		<table>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="companyName"
				<c:if test="${not empty companyName}">value="${companyName}"</c:if>
				></td>
			</tr>
			<tr>
				<td>Estimated Earnings:</td>
				<td><input type="text" name="estimatedEarnings"
				<c:if test="${not empty estimatedEarnings}">value="${estimatedEarnings}"</c:if>
				></td>
			</tr>
			<tr>
				<td>Parent Company:</td>
				<td>
					<select name="parentCompanyId">
					 	<option selected value="0">None</option>
						<c:forEach items="${companies}" var="company"> 
						<option 
						<c:if test="${parentCompanyId == company.getId()}">selected</c:if>
						value="${company.getId()}" >${company.getName()}</option> 
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right" colspan="2"><input type="submit" value="OK"></td>
			</tr>
		</table>
	</form>
