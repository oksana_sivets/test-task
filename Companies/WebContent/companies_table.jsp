<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<script src="js/angular.js"></script>
<script src="js/CompaniesController.js"></script>
  <div ng-app="companiesApp" ng-controller="CompaniesController">
 
	<c:forEach items="${companies}" var="company" >
		<tr>
		<td>${company.getName()}</td>
		<td>${company.getEstimatedEarnings()}</td>
		<td>${companyTree.getCompNameById(company.getParent_id())}</td>
		<td>${company.getTotalEE()}</td>
		<td>
			<button ng-click="delCompany($event)" data-id="${company.getId()}">Delete</button>
			<button ng-click="editCompany($event)" data-id="${company.getId()}">Edit</button>
		</td>
		</tr>
	</c:forEach>

	</div>