var companiesApp = angular.module("companiesApp", []);

companiesApp.directive('tableDirective', function($http, $compile){
	return{
		restrict: 'A',
		scope: true,
		link: function(scope, element, attrs){
			$http.get('/CompanyServlet?target=getTable').success(function(data){
				element.html(data);
				$compile(element.contents())(scope);
			});
		}
	   
	}
});

companiesApp.service('compService', ['$http', '$document', function($http, $document){
	
	return({
		DeleteCompany: DeleteCompany,
		GetCompanies: GetCompanies,
		EditCompany: EditCompany
    });
	
	function DeleteCompany(obj){
		var request = $http({
				 method: 'POST',
				 url: '/CompanyServlet?action=delCompany',
				 headers: {
				   'Content-Type': 'application/x-www-form-urlencoded'
				 },
				 data: obj
		});
		
		return request;
		
	};
	
	function GetCompanies(){
		var request = $http.get('/CompanyServlet?target=getTable')
		return request;
	};
	
	function EditCompany(obj){
		var request = $http({
				 method: 'GET',
				 url: '/CompanyServlet?target=editCompany',
				 params: obj
		});
		
		return request;
		
	}
	
}]);


companiesApp.controller("CompaniesController", function($scope, $document, $http, compService, $httpParamSerializer, $compile) {
	var compTableBody = angular.element($document[0].querySelector('#companies-table-body'));
	var treeContainer = angular.element($document[0].querySelector('#tree-container'));
	var addCompanyForm = angular.element($document[0].querySelector('#add-company-form'));
	var showButton = angular.element($document[0].querySelector('#show-button'));
	$scope.companiesTable = true;
	$scope.companyForm = false;
	$scope.companiesTree = false;
	
	$scope.btnText = ('Show Companies Tree');
		
	$scope.showBtn = function(){
		if($scope.companiesTable == true){
			$http.get('/CompanyServlet?target=getTree').success(function(data){
				treeContainer.html(data);
			});
			$scope.companiesTable = false;
			$scope.companiesTree = true;
			$scope.btnText = ('Show Companies Table');
		}
		else {
			$scope.companiesTable = true;
			$scope.companiesTree = false;
			$scope.btnText = ('Show Companies Tree');
		}
		$scope.companyForm = false;
	};
	
	$scope.showForm = function(){
		$http.get('/CompanyServlet?target=getForm').success(function(data){
			addCompanyForm.html(data);
		});
		if ($scope.companiesTable == true || $scope.companiesTree == true){
			$scope.companiesTable = false;
			$scope.companiesTree = false;
			$scope.companyForm = true;
		}
	};
	
	function UpdateTable(){
		
		compService.GetCompanies().success(function(data){
			compTableBody.html(data);
			$compile(compTableBody.contents())($scope);
	})};
	
	$scope.delCompany = function(event){
		var comp_id = event.currentTarget.getAttribute("data-id");
		var obj = {
				companyId : comp_id
		};
		compService.DeleteCompany($httpParamSerializer(obj)).then(UpdateTable);
				
	}
	
	$scope.editCompany = function(event){
		var comp_id = event.currentTarget.getAttribute("data-id");
		var obj = {
				compId : comp_id
		};
		compService.EditCompany(obj).success(function(data){
			addCompanyForm.html(data);
			});
			
			$scope.companyForm = true;
			$scope.companiesTable = false;
			$scope.companiesTree = false;
						
	}
});
	

